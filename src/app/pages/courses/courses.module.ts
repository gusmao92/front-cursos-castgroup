import { NgModule } from '@angular/core';
import { SharedModule } from "../../shared/shared.module";

import { CoursesRoutingModule } from './courses-routing.module';

import { CourseListComponent } from "./course-list/course-list.component";
import { CourseFormComponent } from "./course-form/course-form.component";

import { CalendarModule } from "primeng/calendar";
import { IMaskModule } from "angular-imask";

@NgModule({
  imports: [
    SharedModule,
    CoursesRoutingModule,
    CalendarModule,
    IMaskModule
  ],
  declarations: [CourseListComponent, CourseFormComponent]
})
export class CoursesModule { }
