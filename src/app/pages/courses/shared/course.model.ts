import {BaseResourceModel} from '../../../shared/models/base-resource.model';
import {Category} from '../../categories/shared/category.model';

export class Course extends BaseResourceModel {
  constructor(
    public id?: number,
    public descricao?: string,
    public dt_inicio?: string,
    public dt_fim?: string,
    public qt_aluno?: number,
    public categoria?: Category
  ) {
    super();
  }

  static fromJson(jsonData: any): Course {
    return Object.assign(new Course(), jsonData);
  }

}
