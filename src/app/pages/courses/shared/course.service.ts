import {Injectable, Injector} from '@angular/core';

import {Observable} from 'rxjs';
import {flatMap, catchError, map} from 'rxjs/operators';

import {BaseResourceService} from '../../../shared/services/base-resource.service';
import {CategoryService} from '../../categories/shared/category.service';
import {Course} from './course.model';

import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class CourseService extends BaseResourceService<Course> {

  constructor(protected injector: Injector, private categoryService: CategoryService) {
    super('/cursos', injector, Course.fromJson);
  }


  create(entry: Course): Observable<Course> {
    return this.setCategoryAndSendToServer(entry, super.create.bind(this));
  }

  update(entry: Course): Observable<Course> {
    return this.setCategoryAndSendToServer(entry, super.update.bind(this));
  }

  private setCategoryAndSendToServer(entry: Course, sendFn: any): Observable<Course> {
    return this.categoryService.getById(entry.id).pipe(
      flatMap(category => {
        entry.categoria = category;
        return sendFn(entry);
      }),
      catchError(this.handleError)
    );
  }
}
