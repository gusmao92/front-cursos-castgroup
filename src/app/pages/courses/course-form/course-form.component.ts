import {Component, OnInit, Injector} from '@angular/core';
import {Validators} from '@angular/forms';

import {BaseResourceFormComponent} from '../../../shared/components/base-resource-form/base-resource-form.component';

import {Course} from '../shared/course.model';
import {CourseService} from '../shared/course.service';

import {Category} from '../../categories/shared/category.model';
import {CategoryService} from '../../categories/shared/category.service';

@Component({
  selector: 'app-entry-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.css']
})
export class CourseFormComponent extends BaseResourceFormComponent<Course> implements OnInit {

  categories: Array<Category>;

  ptBR = {
    firstDayOfWeek: 0,
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    dayNamesMin: ['Do', 'Se', 'Te', 'Qu', 'Qu', 'Se', 'Sa'],
    monthNames: [
      'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho',
      'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'
    ],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    today: 'Hoje',
    clear: 'Limpar'
  };

  private categoriID: number;

  constructor(
    protected courseService: CourseService,
    protected categoryService: CategoryService,
    protected injector: Injector
  ) {
    super(injector, new Course(), courseService, Course.fromJson);
  }

  ngOnInit() {
    this.loadCategories();
    super.ngOnInit();

    console.log(this.resource);
  }

  protected buildResourceForm() {
    this.resourceForm = this.formBuilder.group({
      id: [null],
      descricao: [null],
      qt_aluno: [null, [Validators.required]],
      dt_inicio: [null, [Validators.required]],
      dt_fim: [null, [Validators.required]],
      categoria: [null, [Validators.required]]
    });
  }

  private loadCategories() {
    this.categoryService.getAll().subscribe(
      categories => this.categories = categories
    );
  }

  protected creationPageTitle(): string {
    return 'Cadastro de Novo Lançamento';
  }

  protected editionPageTitle(): string {
    const resourceName = this.resource.descricao || '';
    return 'Editando Lançamento: ' + resourceName;
  }


}
