import {Component} from '@angular/core';

import {BaseResourceListComponent} from '../../../shared/components/base-resource-list/base-resource-list.component';

import {Course} from '../shared/course.model';
import {CourseService} from '../shared/course.service';

@Component({
  selector: 'app-entry-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent extends BaseResourceListComponent<Course> {

  constructor(private courseService: CourseService) {
    super(courseService);
  }
}
